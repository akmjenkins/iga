<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Leadership</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>The IGA is dedicated to keeping the spirit of service alive in Newfoundland and Labrador through the 21st century.</p>
									 
									<p>Meet the people who continue to keep Sir Grenfell’s mission going today.</p>
									
								</div><!-- .article-body -->
								
						
								<div class="overview-blocks">
									
									<div class="overview-block">
										<div class="img" style="background-image: url(assets/bin/images/temp/norm.jpg);">
											<img src="assets/bin/images/temp/norm.jpg" alt="norm" class="lazy">
										</div><!-- .img -->
										<div class="content">
											<div class="hgroup">
												<h2>Dr. Norman Pinder, GAGBI</h2>
												<span class="subtitle">Chairman</span>
											</div><!-- .hgroup -->
											
											<p>
												Dr. Pinder, a resident of Norwich, England, is a retired public health physician. He chairs the IGA Executive Committee, 
												Scrutiny Committee, and is also a member of the Nominating Committee.
											</p>
											
										</div>
									</div><!-- .overview-block -->

									<div class="overview-block">
										<div class="img" style="background-image: url(assets/bin/images/temp/keat.jpg);">
											<img src="assets/bin/images/temp/keat.jpg" alt="keat" class="lazy">
										</div><!-- .img -->
										<div class="content">
											<div class="hgroup">
												<h2>Mr. Keating Hagmann, GAA</h2>
												<span class="subtitle">Vice Chairman</span>
											</div><!-- .hgroup -->
											
											<p>
												Mr. Hagmann is a financial advisor in New York City, and is a resident of Cos Cob, Connecticut. 
												He chairs the IGA Investment Committee, and is a member of the Nominating Committee, Audit Committee, and Executive Committee.
											</p>
											
										</div>
									</div><!-- .overview-block -->

									<div class="overview-block">
										<div class="img" style="background-image: url(assets/bin/images/temp/donna.jpg);">
											<img src="assets/bin/images/temp/donna.jpg" alt="donna" class="lazy">
										</div><!-- .img -->
										<div class="content">
											<div class="hgroup">
												<h2>Ms. Donna Jeanloz, NEGA</h2>
											</div><!-- .hgroup -->
											
											<p>
												Ms. Jeanloz is a resident of Millers Fall, Massachusetts, where she is a business owner and homemaker. 
												Donna Jeanloz is chair of NEGA, and sits on the IGA Scholarship Committee.
											</p>
											
										</div>
									</div><!-- .overview-block -->
									
								</div><!-- .overview-blocks -->

							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Our History</a>
										<a href="#">Our Role</a>
										<a href="#">Our Impact</a>
										<a href="#" class="selected">Leadership</a>
										<a href="#">Initiatives</a>
										<a href="#">Annual Reports</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>