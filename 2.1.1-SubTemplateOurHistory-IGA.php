<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Who We Are</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured-2">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>
										More than a century ago, British surgeon and medical missionary Dr. Wilfred T. Grenfell came to this province with one goal: 
										to serve the many needs of the coastal settlements of Labrador and Northern Newfoundland, especially the fishermen who 
										lived and worked in these areas. Today, Sir Grenfell’s legacy lives on in the International Grenfell Association (IGA), which 
										provides support for local communities through funding for a variety of non-profit organizations.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#" class="selected">Our History</a>
										<a href="#">Our Role</a>
										<a href="#">Our Impact</a>
										<a href="#">Leadership</a>
										<a href="#">Initiatives</a>
										<a href="#">Annual Reports</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
						
					<?php include('inc/i-timeline.php'); ?>
						
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>