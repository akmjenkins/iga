define([
	'scripts/components/swipe',
],function(Swipe) {

	var
		el = $('section.the-latest');
		
		//keep the "ghost" and "real" list item classes in sync
		updateSelectedListItem = function() {
			var
				indexOfHighlightedLiParent,
				indexOfHighlightedGhostLiParent,
				listSwiper = getListSwiper(),
				i = getArticleSwiper().getPos(),
				lis = getListSwiperEl().find('li'),
				ghostLis = getListSwiperEl().find('div.ghost').find('li');
			
			lis.removeClass('selected');
			ghostLis.removeClass('selected');
			
			lis.eq(i).addClass('selected');
			ghostLis.eq(i).addClass('selected');
			
			//make sure the highlighted link is visible
			indexOfHighlightedLiParent = lis.eq(i).closest('div.slide').index();
			indexOfHighlightedGhostLiParent = ghostLis.eq(i).closest('div.slide').index();
			
			( listSwiper.getPos() != indexOfHighlightedLiParent && listSwiper.getPos() != indexOfHighlightedGhostLiParent ) && listSwiper.slide(indexOfHighlightedLiParent);
			
		},
		
		//called when article swiper has changed - keeps appropriate item in list highlighted
		doSelectListItem = function(i) {
			updateSelectedListItem();
		
			var 
				el = $(this),
				li = getListSwiperEl().find('li').eq(i);
				
			if(li.hasClass('selected')) { return; }
			
			getListSwiper().slide(li.closest('div.slide').index());
				
		},
		
		//called when a list item has been clicked
		didSelectListItem = function(el) {
			
			var 
				lis = (el.closest('div.ghost').length) ? getListSwiperEl().find('div.ghost li') : getListSwiperEl().find('li'),
				index = lis.index(el);
				
			if(el.hasClass('selected')) { return; }
			
			getArticleSwiper().slide(index);
		},
		
		//contents may be reloaded via ajax - use accessor methods to get elements and sliders
		getListSwiperEl = function() {
			return $('div.the-latest-side-slider div.swipe',el);
		},
		getListSwiper = function() {
			return getListSwiperEl().data('swipe');
		},
		getArticleSwiperEl = function() {
			return $('div.the-latest-article div.swipe',el);
		},
		getArticleSwiper = function() {
			return getArticleSwiperEl().data('swipe');
		};
		
		
	$(document)
		.on('swipeChanged','div.the-latest-article',function(e,i) {
			doSelectListItem(i);
		})
		.on('click','div.the-latest-side-slider li',function() {
			didSelectListItem($(this));
		});

	//no public API
	return {};

});