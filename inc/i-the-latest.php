					<section class="the-latest">
						
						<div class="section-title">
							<h2>The Latest</h2>
							<span class="subtitle">Phasellus dui purus, dignissim nec pellentesque vel, egestas et nibh.</span>
						</div><!-- .section-title -->
						
						<div class="the-latest-filters">
							
							<a href="#" class="button">Grenfell</a>
							
							<div class="selector">
								<select name="the-latest-region" id="the-latest-region">
									<option value="">Region</option>
									<option value="np">Nothern Peninsula</option>
									<option value="ls">Labrador Straits</option>
									<option value="ls">Labrador South Coast</option>
									<option value="ls">Central Labrador</option>
									<option value="ls">Labrador North Coast</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
							
						</div><!-- .the-latest-filters -->
						
						<div class="the-latest-content">
						
							<div class="the-latest-side-slider">
								
								<div class="swiper-wrapper">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div class="slide">											
												<div class="list-wrap">
													<ul>
														<li class="selected">
															
															<time class="light" datetime="2014-01-24">
																<span>24</span> Jan
															</time>
															
															<p>
																Donec rhoncus ipsum et libero tempus, in semper lorem varius. Donec nec gravida turpis. vitae nisl ut eros molestie mattis 
															</p>
															
														</li>
														
														<li>
															
															<time class="light" datetime="2014-01-24">
																<span>07</span> Jun
															</time>
															
															<p>
																Cras luctus porttitor imperdiet. In imperdiet lectus at massa commodo ullamcorper.
															</p>
															
														</li>
														
														<li>
															
															<time class="light" datetime="2014-01-24">
																<span>29</span> Apir
															</time>
															
															<p>
																Donec rhoncus ipsum et libero tempus, in semper lorem varius. Fusce in tristique.
															</p>
															
														</li>
													</ul>
												</div><!-- .list-wrap -->
											</div>
											<div class="slide">					
												<div class="list-wrap">
													<ul>
														<li>
															
															<time class="light" datetime="2014-01-24">
																<span>24</span> Jan
															</time>
															
															<p>
																Donec rhoncus ipsum et libero tempus, in semper lorem varius. Donec nec gravida turpis. vitae nisl ut eros molestie mattis 
															</p>
															
														</li>
													</ul>
												</div><!-- .list-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
									
									<a href="#" class="right button">View All</a>
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .the-latest-side-slider -->
							
							<div class="the-latest-article">
								
								<div class="swiper-wrapper">
									<div class="swipe">
										<div class="swipe-wrap">
											<div>
												
												<article>
													<img src="assets/bin/images/temp/the-latest.jpg" class="alignleft">
													
													<div class="article-body">
														<strong class="uc">First Slide</strong>
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, 
														porttitor imperdiet arcu.</p>
														
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, porttitor 
														imperdiet arcu. Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et 
														tristique mollis, tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque 
														enim, porttitor imperdiet arcu.</p>
														
														<hr />
														
														<a href="#" class="button right">Read More</a>
													</div><!-- .article-content -->
													
												</article>
												
											</div>
											<div>
												
												<article>
													<img src="assets/bin/images/temp/the-latest.jpg" class="alignleft">
													
													<div class="article-body">
														<strong class="uc">Second Slide</strong>
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, 
														porttitor imperdiet arcu.</p>
														
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, porttitor 
														imperdiet arcu. Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et 
														tristique mollis, tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque 
														enim, porttitor imperdiet arcu.</p>
														
														<hr />
														
														<a href="#" class="button right">Read More</a>
													</div><!-- .article-content -->
													
												</article>
												
											</div>
											<div>
												
												<article>
													<img src="assets/bin/images/temp/the-latest.jpg" class="alignleft">
													
													<div class="article-body">
														<strong class="uc">Third Slide</strong>
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, 
														porttitor imperdiet arcu.</p>
														
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, porttitor 
														imperdiet arcu. Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et 
														tristique mollis, tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque 
														enim, porttitor imperdiet arcu.</p>
														
														<hr />
														
														<a href="#" class="button right">Read More</a>
													</div><!-- .article-content -->
													
												</article>
												
											</div>
											<div>
												
												<article>
													<img src="assets/bin/images/temp/the-latest.jpg" class="alignleft">
													
													<div class="article-body">
														<strong class="uc">FourthSlide</strong>
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, 
														porttitor imperdiet arcu.</p>
														
														<p>Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et tristique mollis, 
														tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque enim, porttitor 
														imperdiet arcu. Maecenas posuere vitae sapien at egestas. Curabitur consectetur, sapien et 
														tristique mollis, tellus eros eleifend arcu, nec pharetra nisi risus id ligula. Proin at pellentesque 
														enim, porttitor imperdiet arcu.</p>
														
														<hr />
														
														<a href="#" class="button right">Read More</a>
													</div><!-- .article-content -->
													
												</article>
												
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .the-latest-article -->
						
						</div><!-- .the-latest-content -->
						
					</section><!-- .the-latest -->
