<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Annual Reports</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured-2">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<blockquote>
										More than a century ago, British surgeon and medical missionary Dr. Wilfred T. Grenfell came to this province with one goal: 
										to serve the many needs of the coastal settlements of Labrador and Northern Newfoundland, especially the fishermen who 
										lived and worked in these areas. Today, Sir Grenfell’s legacy lives on in the International Grenfell Association (IGA), which 
										provides support for local communities through funding for a variety of non-profit organizations.
									</blockquote>
									
									<p>
										The reports shown below are examples of reports containing the type of information, as well as level of detail, that the IGA would like to have submitted.
									</p>
								</div><!-- .article-body -->
									
								<div class="accordion separated allow-multiple">
								
									<div class="accordion-item">
										<div class="accordion-item-handle lg">
											Community Grant Reports
										</div><!-- .accordion-item-handle -->
										<div class="accordion-item-content">
											
											<ul>
												<li><a href="#" class="sprite-before download">Cook's Harbour Recreation Committee</a></li>
												<li><a href="#" class="sprite-before download">Partners in Personal Growth</a></li>
												<li><a href="#" class="sprite-before link" rel="external">Them Days Incorporated</a></li>
											</ul>
											
										</div><!-- .accordion-item-content -->
									</div><!-- .accordion-item -->
									
									<div class="accordion-item">
										<div class="accordion-item-handle lg">
											Education Grant Reports
										</div><!-- .accordion-item-handle -->
										<div class="accordion-item-content">
										
											<ul>
												<li><a href="#" class="sprite-before download">Environmental Immersion Camps</a></li>
												<li><a href="#" class="sprite-before link" rel="external">Building School Literacy Collections</a></li>
												<li><a href="#" class="sprite-before download">Labrador Leadership Program</a></li>
											</ul>
											
										</div><!-- .accordion-item-content -->
									</div><!-- .accordion-item -->
									
									<div class="accordion-item">
										<div class="accordion-item-handle lg">
											Health Grant Reports
										</div><!-- .accordion-item-handle -->
										<div class="accordion-item-content">
										
											<ul>
												<li><a href="#" class="sprite-before download">Environmental Immersion Camps</a></li>
												<li><a href="#" class="sprite-before link" rel="external">Building School Literacy Collections</a></li>
												<li><a href="#" class="sprite-before download">Labrador Leadership Program</a></li>
											</ul>
											
										</div><!-- .accordion-item-content -->
									</div>
									
								</div><!-- .accordion -->
									
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Our History</a>
										<a href="#">Our Role</a>
										<a href="#">Our Impact</a>
										<a href="#">Leadership</a>
										<a href="#">Initiatives</a>
										<a href="#" class="selected">Annual Reports</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
						
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>