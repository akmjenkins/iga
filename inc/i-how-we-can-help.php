					<section class="how-can-we-help">
						<div>
							<div>
								<h2>How Can We Help?</h2>
								<p>Type your question, keyword or phrase into our search box to quickly find what you are looking for.</p>
							</div>
							
							<div>						
								<form action="/" class="single-form">
									<fieldset>
										<span class="num">15</span>
										<input type="text" name="help">
										<button type="button" class="sprite search-ico-dark" title="Search IGA Help">Search IGA Help</button>
									</fieldset>
								</form>
							</div>
						</div><!-- .how-we-can-help -->
					</section><!-- .how-can-we-help -->
					
					<div class="filter-area with-close with-swiper">
						
						<div class="filter-bar">
						
							<span class="label">
								Search for <strong>"Where are my pants?"</strong>
							</span>
						
							<span class="label">
								8 Items Found
							</span><!-- .label -->
							
							<a href="#" class="sprite-after abs close">Close</a>
						</div>
						
						<div class="filter-content">
							
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>

												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->														
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											
											</div>
											<div>

												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->														
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											
											</div>
											<div>

												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->														
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
							
							</div><!-- .news-stories-grid -->
							
						</div><!-- .filter-content -->
						
					</div><!-- .filter-area -->

