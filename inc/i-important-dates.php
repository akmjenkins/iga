					<section class="with-flowing-bg dates">
						
						<p>Phasellus dui purus, dignissim nec pellentesque vel, egestas et nibh.</p>
						
						<div class="grid-eqh-full-wrap collapse-750">
							<div class="grid-eqh-wrap">
								<div class="grid-eqh">
								
									<div class="col-33">
										<div>
											<time class="light" datetime="2014-01-24">
												<span>24</span> Jan
											</time>
											
											<p>Closing date for applications: February 20, 2014</p>
										
											<a href="#">Read More</a>
										</div>
									</div><!-- .col -->
									
									<div class="col-33">
										<div>
											<time class="light" datetime="2014-01-24">
												<span>24</span> Jan
											</time>
											
											<p>Closing date for applications: February 20, 2014</p>
										
											<a href="#">Read More</a>
										</div>
									</div><!-- .col -->
									
									<div class="col-33">
										<div>
											<time class="light" datetime="2014-01-24">
												<span>24</span> Jan
											</time>
											
											<p>Closing date for applications: February 20, 2014</p>
										
											<a href="#">Read More</a>
										</div>
									</div><!-- .col -->
									
								</div><!-- .grid-eqh -->
							</div><!-- .grid-eqh-wrap -->
						</div><!-- .grid-eqh-full-wrap -->
						
					</section><!-- .dates -->
