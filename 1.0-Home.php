<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>
				
<?php include('inc/i-hero-swiper.php'); ?>
				
				<div class="body">

					<?php include('inc/i-who-we-are.php'); ?>
				
					<?php include('inc/i-how-we-can-help.php'); ?>
				
					<?php include('inc/i-important-dates.php'); ?>
				
					<?php include('inc/i-the-latest.php'); ?>
				
					<div class="is-overlay">
						<?php include('inc/i-map.php'); ?>
					</div>
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>