<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Pellentesque ut Sapien Fringilla </h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
							
								<div class="article-body">
								
									<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus interdum tellus tincidunt elit viverra egestas. Suspendisse nulla libero, 
									posuere eget magna nec, porta suscipit felis. Morbi egestas suscipit posuere. Sed at diam nisi. Integer blandit molestie aliquam.</p>

									<p>In auctor tortor quis vestibulum placerat. Vestibulum placerat ante est, at venenatis tortor molestie at. Donec rutrum turpis sit amet dignissim laoreet. 
									Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus convallis congue nibh, id blandit lorem gravida eget. 
									Donec diam arcu, porta vitae pharetra ut, fermentum vel ante. Ut sagittis, ligula eget lobortis ornare, enim sem faucibus risus, eget dignissim eros sapien 
									quis neque. Curabitur sapien est, adipiscing vel dignissim ac, euismod in dui. In mi risus, cursus et varius ac, dictum vitae urna. Maecenas at orci ut ipsum 
									aliquet eleifend. Praesent malesuada arcu ac nisl ultricies luctus. Phasellus pretium ut quam ut fringilla. Cras vitae augue eleifend, aliquam elit a, 
									volutpat urna.</p>

									<p>Vivamus dapibus volutpat metus, nec porttitor risus semper id. Ut blandit sed ipsum id ultricies. Aenean volutpat tellus eget mauris volutpat laoreet. Aenean 
									imperdiet lorem risus, at tristique justo sagittis quis. Suspendisse sed turpis lorem. Curabitur dignissim eros non odio euismod, et rutrum orci consequat. Vivamus 
									interdum rhoncus tortor vulputate gravida. Ut justo risus, rhoncus commodo bibendum rutrum, pellentesque in odio. Cras blandit lacus lorem, in blandit magna 
									gravida et.</p>

									<p>Donec bibendum augue at eros semper, sed consequat nulla porta. Fusce blandit ac lectus sit amet imperdiet. Nullam non ipsum auctor, venenatis dui id, 
									elementum magna. Integer vel feugiat tortor, in aliquet purus. Proin malesuada quam nec mi hendrerit, nec varius nisl pellentesque. Duis hendrerit augue nisi, 
									ac interdum tortor pharetra sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur 
									ridiculus mus. Pellentesque sagittis tellus lorem, quis facilisis libero condimentum quis. Sed non nisl aliquet, sodales nibh eget, pulvinar est.</p>

									<p>Sed scelerisque mauris nec lectus elementum, non dignissim felis fringilla. Suspendisse a nunc id massa porta cursus vitae id metus. Mauris porttitor sed lectus 
									non ornare. Fusce non dignissim mauris.</p>

								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Back To Stories</a>
										<div>
											<a href="#" class="prev">Previous Story</a>
											<a href="#" class="next">Next Story</a>
										</div>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->

					</article>

				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>