<!doctype html>
<!--[if IE 8 ]>    <html lang="en" class="ie8 resp"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9 resp"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="resp"> <!--<![endif]--> 

	<head>
		<title>International Grenfell Association - Projects</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" href="assets/bin/css/style.css?<?php echo time(); ?>">
		
		<!-- modernizr -->
		<script src="assets/bin/js/lib/modernizr/modernizr.js"></script>
		
		<!-- jQuery -->
		<!--[if IE 8 ]>
			<script src="assets/bin/lib/js/jquery-legacy/jquery.min.js"></script>	
		<![endif]--> 
		<!--[if (gte IE 9)|!(IE)]><!-->
			<script src="assets/bin/lib/js/jquery/jquery.min.js"></script>	
		<!--<![endif]--> 
		
		<!-- favicons -->
		<link rel="icon" type="image/png"  href="assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" type="image/x-icon"  href="assets/bin/images/favicons/favicon.ico">
		<link rel="icon" href="assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon" sizes="152x152" href="assets/bin/images/favicons/apple-touch-152.png">
		<link rel="apple-touch-icon" sizes="144x144" href="assets/bin/images/favicons/apple-touch-144.png">
		<link rel="apple-touch-icon" sizes="120x120" href="assets/bin/images/favicons/apple-touch-120.png">
		<link rel="apple-touch-icon" sizes="114x114" href="assets/bin/images/favicons/apple-touch-114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="assets/bin/images/favicons/apple-touch-144.png">
		<link rel="apple-touch-icon" href="assets/bin/images/favicons/apple-touch-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="assets/bin/images/favicons/apple-touch-144.png">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
	</head>
	<body class="map-page">
		<?php include('inc/i-map.php'); ?>
	
		<script>
			var templateJS = {
				templateURL: 'http://jac.dev/iga/',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
	
		<script data-main="assets/bin/js/main.js?<?php echo time(); ?>" src="assets/bin/lib/js/requirejs/require.min.js"></script>
	
	</body>
</html>