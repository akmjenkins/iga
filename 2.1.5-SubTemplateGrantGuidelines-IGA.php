<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Grant Guidelines</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>The International Grenfell Association was once the major organization providing health care, 
									education, religious services, and other social activities to the fishing industries and costal 
									communities in northern Newfoundland and coastal Labrador.</p>
 
									<p>The IGA prides itself on continuing the tradition of excellence in community care that 
									Sir Grenfell brought with him to the shores of this province.</p>
									
									<h2>Types of Bursaries</h2>
									
									<div class="grid collapse-599">
									
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/equity-ico.png" class="aligncenter" alt="equity">
												<span class="h3-style">Equity</span>
												<p>Targeting resources to create balance, trying to respond evenly for the same need over time.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/relevance-ico.png" class="aligncenter" alt="relevance">
												<span class="h3-style">Relevance</span>
												<p>A project that is meaningful, focused, and in line with existing needs.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/equity-ico.png" class="aligncenter" alt="equity">
												<span class="h3-style">Appropriateness</span>
												<p>A timely project, fitting with current circumstances and conditions.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
									<div class="grid collapse-599">
									
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/search-ico.png" class="aligncenter" alt="search">
												<span class="h3-style">Evidenced Based</span>
												<p>A project that has been shown to work effectively, and/or has precedence.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/innovation-ico.png" class="aligncenter" alt="innovation">
												<span class="h3-style">Innovation</span>
												<p>Breaking new ground, offering new ways of doing things.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/people-ico.png" class="aligncenter" alt="people">
												<span class="h3-style">Accountability</span>
												<p>A project can be justified, with responsible actions.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
									<div class="grid collapse-599">
									
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/money-ico.png" class="aligncenter" alt="money">
												<span class="h3-style">Cost Benefit</span>
												<p>A project that offers a good return for the investment.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/time-scale-ico.png" class="aligncenter" alt="time">
												<span class="h3-style">Time Scale</span>
												<p>A project is achievable within the indicated time period.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/review-ico.png" class="aligncenter" alt="review">
												<span class="h3-style">Annual Review</span>
												<p>A project is open to evaluation by the IGA board.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
									<p>In general, the IGA will avoid projects that:</p>
									
									<ul>
										<li> continue a defunct program</li>
										<li> provide for general maintenance and upkeep</li>
										 <li>last three years or more</li>
										 <li>contain salary costs for permanent staff</li>
										 <li>provide supplies normally funded by government</li>
										 <li>contain evidence of direct private benefit</li>
										 <li>cover costs of warranties and/or service contracts on equipment</li>
									 </ul>
									 
									<p>
										It is to your advantage to provide as much detail as possible in a clear, concise manner. 
										Without adequate information, the IGA has no alternative but to return your application, causing 
										delay. Therefore, if necessary, you may attach additional papers to support your application.
									</p>
									 
									<p>
										If your project application is successful, the IGA may award funds for the full amount, or a portion of that 
										requested. In some instances, matching funds from other sources may be required by the IGA.
									</p>
									 
									<p>
										**Please note, failure to observe the conditions on which the grant is made will lead to a claim for funds to be 
										returned and will disqualify the recipient from receiving future IGA awards.
									</p>
									
									<h2>Applying</h2>
									
									<p>
										Annual requests for funding may be by completing applications on or before November 1. To apply for a grant, 
										download the grant application form and submit to the IGA General Business Office in person, by mail, or by email. 
										Applicants are reminded to closely follow the guidance notes when preparing their application.
									</p>
									
									<blockquote>
										Please note: All applications must be received by November 1 at the IGA General Business Office. Under no circumstances 
										will applications post-marked after this date be accepted and/or considered. Please do not fax your application.
									</blockquote>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#" class="selected">Grant Guidlines</a>
										<a href="#">Current Grants</a>
										<a href="#">Completed Grants</a>
										<a href="#">Forms</a>
									</div><!-- .related-links -->
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>