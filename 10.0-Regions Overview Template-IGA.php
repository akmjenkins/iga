<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
							
						<div class="hgroup">
							<h1>Regions</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-3.jpg">
						</div>
						
						<div class="cf">
							<div class="main-body">
								<div class="article-body">
								
									<p class="centered-excerpt">
										In cursus tempor enim et accumsan. Fusce et vulputate risus. Pellentesque interdum facilisis purus, ornare aliquam 
										velit fermentum sit amet. Donec vehicula nisl ac diam condimentum, nec scelerisque nunc elementum. Cras tincidunt 
										viverra purus mattis aliquam. In a velit pellentesque, commodo magna vitae, dapibus quam. Maecenas sapien sem, convallis sed turpis eu, sagittis tristique dui.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body -->
						</div><!-- .cf -->
						
						<div class="overview-blocks">
							
							<div class="overview-block">								
								<div class="img" style="background-image: url(http://maps.googleapis.com/maps/api/staticmap?center=50.81,-55.67&zoom=8&size=400x400);">
									<img alt="map" class="lazy" src="http://maps.googleapis.com/maps/api/staticmap?center=50.81,-55.67&zoom=8&size=400x400">
									<!-- <div class="map" data-zoom="7" data-center="50.81,-55.67" data-markers='[]'></div> -->
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Northern Peninsula</h2>
										<span class="subtitle">Cras Venenatis Metus a Libero Gravida</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat. 
										Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet. Etiam ut ligula rhoncus, fringilla turpis sit amet, 
										fermentum massa. 
									</p>
									
									<a href="#" class="button white">More Details</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block">
								<div class="img" style="background-image: url(http://maps.googleapis.com/maps/api/staticmap?center=51.90,-55.84&zoom=8&size=400x400);">
									<img alt="map" class="lazy" src="http://maps.googleapis.com/maps/api/staticmap?center=51.90,-55.84&zoom=8&size=400x400">
									<!-- <div class="map" data-zoom="7" data-center="51.90,-55.84" data-markers='[]'></div> -->
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Labrador Straits</h2>
										<span class="subtitle">Cras Venenatis Metus a Libero Gravida</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat. 
										Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet. Etiam ut ligula rhoncus, fringilla turpis sit amet, 
										fermentum massa. 
									</p>
									
									<a href="#" class="button white">More Details</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block">								
								<div class="img" style="background-image: url(http://maps.googleapis.com/maps/api/staticmap?center=53.18,-56.74&zoom=8&size=400x400);">
									<img alt="map" class="lazy" src="http://maps.googleapis.com/maps/api/staticmap?center=53.18,-56.74&zoom=8&size=400x400">
									<!-- <div class="map" data-zoom="7" data-center="53.18,-56.74" data-markers='[]'></div> -->
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Labrador South Coast</h2>
										<span class="subtitle">Cras Venenatis Metus a Libero Gravida</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat. 
										Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet. Etiam ut ligula rhoncus, fringilla turpis sit amet, 
										fermentum massa. 
									</p>
									
									<a href="#" class="button white">More Details</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block">								
								<div class="img" style="background-image: url(http://maps.googleapis.com/maps/api/staticmap?center=53.63,-59.43&zoom=8&size=400x400);">
									<img alt="map" class="lazy" src="http://maps.googleapis.com/maps/api/staticmap?center=53.63,-59.43&zoom=8&size=400x400">
									<!-- <div class="map" data-zoom="7" data-center="53.63,-59.43" data-markers='[]'></div> -->
								</div><!-- .img -->
								
									
								<div class="content">
									<div class="hgroup">
										<h2>Central Labrador</h2>
										<span class="subtitle">Cras Venenatis Metus a Libero Gravida</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat. 
										Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet. Etiam ut ligula rhoncus, fringilla turpis sit amet, 
										fermentum massa. 
									</p>
									
									<a href="#" class="button white">More Details</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block">								
								<div class="img" style="background-image: url(http://maps.googleapis.com/maps/api/staticmap?center=56.47,-59.97&zoom=6&size=400x400);">
									<img alt="map" class="lazy" src="http://maps.googleapis.com/maps/api/staticmap?center=56.47,-59.97&zoom=6&size=400x400">
									<!-- <div class="map" data-zoom="7" data-center="56.47, -59.97" data-markers='[]'></div> -->
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Labrador North Coast</h2>
										<span class="subtitle">Cras Venenatis Metus a Libero Gravida</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat. 
										Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet. Etiam ut ligula rhoncus, fringilla turpis sit amet, 
										fermentum massa. 
									</p>
									
									<a href="#" class="button white">More Details</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
						</div><!-- .overview-blocks -->
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>