<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Lorem Ipsum Dolor Sit Amet</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
							
						
								<div class="event-meta-bar dblue-bg">
								
									<time class="blocks" datetime="2014-06-24">
										<span class="day">24</span>
										<span class="month-year">
											<span class="month">Jun</span>
											<span class="year">2014</span>
										</span>
									</time><!-- .blocks -->
									
									<span class="sprite-before time">6:00pm</span>
									<span class="sprite-before map">This Location, This Town</span>
									
								</div><!-- .event-meta -->

							
								<div class="article-body">
								
									<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus interdum tellus tincidunt elit viverra egestas. 
									Suspendisse nulla libero, posuere eget magna nec, porta suscipit felis. Morbi egestas suscipit posuere. 
									Sed at diam nisi. Integer blandit molestie aliquam.</p>

									<p>In auctor tortor quis vestibulum placerat. Vestibulum placerat ante est, at venenatis tortor molestie at. Donec rutrum turpis 
									sit amet dignissim laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus 
									convallis congue nibh, id blandit lorem gravida eget. Donec diam arcu, porta vitae pharetra ut, fermentum vel ante. Ut sagittis, 
									ligula eget lobortis ornare, enim sem faucibus risus, eget dignissim eros sapien quis neque.</p>

									<p>Curabitur sapien est, adipiscing vel dignissim ac, euismod in dui. In mi risus, cursus et varius ac, dictum vitae urna. Maecenas 
									at orci ut ipsum aliquet eleifend. Praesent malesuada arcu ac nisl ultricies luctus. Phasellus pretium ut quam ut fringilla. Cras vitae 
									augue eleifend, aliquam elit a, volutpat urna.</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Back To Events</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->

					</article>

				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>