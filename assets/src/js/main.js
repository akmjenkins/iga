//load all required scripts
requirejs(
	[
	
		'map',
	
		'scripts/components/grid.heights.firefox',
		'scripts/components/gmap',
		'scripts/components/custom.select',
		'scripts/components/standard.accordion',
		'scripts/components/responsive.nav',
		'scripts/components/lazy.img',
		
		'scripts/search.form',
		'scripts/swipe.srf',
		'scripts/the.latest',
		'scripts/accessibility',
	],
	function(ProjectMap) {
	
		//expose globally in namespace
		window.iga = {};
		window.iga.ProjectMap = ProjectMap;
		
		$(document)
			.on('click','.show-project-map',function(e) {
				var region = $(this).data('region');
				e.preventDefault();
				
				if(region) {
					ProjectMap.showWithRegion(region);
				} else {
					ProjectMap.show();
				}
				
				
			});
	
	}
);