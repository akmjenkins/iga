<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
							
						<div class="hgroup">
							<h1>Contact</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-4.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body">
								<div class="article-body">
								
									<p class="centered-excerpt">
										Pellentesque interdum facilisis purus, ornare aliquam velit fermentum sit amet. Donec vehicula 
										nisl ac diam condimentum, nec scelerisque nunc elementum. Cras tincidunt viverra purus mattis aliquam. 
										In a velit pellentesque, commodo magna vitae, dapibus quam. Maecenas sapien sem, convallis sed turpis 
										eu, sagittis tristique dui. Curabitur vulputate tortor nibh, ac feugiat risus sollicitudin id. 
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body -->
						</div><!-- .cf -->
						
						<div class="contact-page">
							<div class="grid center vcenter collapse-750">
								<div class="col-40">
								
									<div class="contact-info">
								
										<strong class="block">Address</strong>
										<address>
											International Grenfell Association <br />
											81 Kenmount Road, 2nd Floor <br />
											St. John's, NL, Canada <br />
											A1B 3P8
										</address>
										
										<br />
										
										<span class="block">Tel: (709) 745-6162</span>
										<span class="block">Fax: (709) 745-6163</span>
										<span class="block"><a href="mailto:iga@nfld.net">iga@nfld.net</a></span>
									
										<div class="social-circles">
											<?php include('inc/i-social.php'); ?>
										</div><!-- .social-circles -->
									
									</div><!-- .contact-info -->
								
								</div><!-- .col -->
								<div class="col-60">
									
									<form action="/" method="post" class="body-form">
										<fieldset>
											
											<label class="row">
												<input type="text" name="name" class="field">
												<span>Name</span>
											</label><!-- .row -->
											
											<label class="row">
												<input type="email" name="email" class="field">
												<span>Email</span>
											</label><!-- .row -->
											
											<label class="row">
												<input type="tel" pattern="\d+" name="tel" class="field">
												<span>Phone</span>
											</label><!-- .row -->
											
											<label class="row">
												<textarea name="message" class="field"></textarea>
												<span>Message</span>
											</label><!-- .row -->
											
											<button class="button" type="submit">Submit</button>
											
										</fieldset>
									</form><!-- .body-form -->
									
								</div><!-- .col -->
							</div><!-- .vcenter -->
						</div><!-- .contact-page -->
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>