<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Initiatives</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-4.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>
										Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidunt sapien ullamcorper at. 
										Quisque mollis neque ultrices orci varius rhoncus. Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
										Fusce nec facilisis lorem, id posuere mi.
									</p>
									
									<p>
										Praesent a interdum massa, eget convallis massa. Donec luctus urna quis mauris egestas, a tincidunt mauris dignissim. 
										Duis a nunc non est blandit molestie. Etiam placerat tristique nulla, et sollicitudin augue auctor.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Our History</a>
										<a href="#">Our Role</a>
										<a href="#">Our Impact</a>
										<a href="#">Leadership</a>
										<a href="#" class="selected">Initiatives</a>
										<a href="#">Annual Reports</a>
									</div><!-- .related-links -->
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
					
					<div class="filter-area extra-margin with-form with-swiper">
						
						<div class="filter-bar">
						
							<span class="label">
								8 Items Found
							</span><!-- .label -->
							
							<div class="selector">
								<select name="filter-selector" id="filter-selector">
									<option value="">Region</option>
									<option value="np">Nothern Peninsula</option>
									<option value="ls">Labrador Straits</option>
									<option value="ls">Labrador South Coast</option>
									<option value="ls">Central Labrador</option>
									<option value="ls">Labrador North Coast</option>
								</select>
								<span class="value">Region</span>
							</div><!-- .selector -->
							
							<form action="/" class="filter-form single-form">
								<fieldset>
									<input type="text" name="filter" placeholder="Search forms...">
									<button class="sprite search-ico" title="Search forms...">Search forms...</button>
								</fieldset>
							</form><!-- .single-form -->
							
						</div><!-- .filter-bar.with-form -->
						
						<div class="filter-content with-links">
							
							<div class="links">
								<a href="#" class="sprite previous">Previous</a>
								<a href="#" class="sprite next">Next</a>
							</div><!-- .links -->
							
							<div class="overview-blocks">
						
								<div class="overview-block with-grad with-time">
								
									<time class="blocks white" datetime="2014-06-24">
										<span class="day">24</span>
										<span class="month-year">
											<span class="month">Jun</span>
											<span class="year">2014</span>
										</span>
									</time>
								
									<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-6.jpg); ">
										<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-6.jpg" class="lazy">
									</div><!-- .img -->
									<div class="content">
										<div class="hgroup">
											<h2>Vivamus Dapibus Volutpat Metus</h2>
											<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
										</div><!-- .hgroup -->
										
										<p>
											Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus interdum tellus tincidunt elit viverra egestas. 
											Suspendisse nulla libero, posuere eget magna nec, porta suscipit felis. Morbi egestas suscipit posuere. Sed at diam nisi. 
											Integer blandit molestie aliquam.
										</p>
										
										<a href="#" class="button white">Continue Reading</a>
										
										<span class="region-tag">Northern Peninsula</span>
									</div><!-- .content -->
								</div><!-- .overview-block -->

								<div class="overview-block with-grad with-time">
								
									<time class="blocks white" datetime="2014-06-24">
										<span class="day">24</span>
										<span class="month-year">
											<span class="month">Jun</span>
											<span class="year">2014</span>
										</span>
									</time>
								
									<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-6.jpg); ">
										<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-6.jpg" class="lazy">
									</div><!-- .img -->
									<div class="content">
										<div class="hgroup">
											<h2>Vivamus Dapibus Volutpat Metus</h2>
											<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
										</div><!-- .hgroup -->
										
										<p>
											Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus interdum tellus tincidunt elit viverra egestas. 
											Suspendisse nulla libero, posuere eget magna nec, porta suscipit felis. Morbi egestas suscipit posuere. Sed at diam nisi. 
											Integer blandit molestie aliquam.
										</p>
										
										<a href="#" class="button white">Continue Reading</a>
										
										<span class="region-tag">Northern Peninsula</span>
									</div><!-- .content -->
								</div><!-- .overview-block -->

								<div class="overview-block with-grad with-time">
								
									<time class="blocks white" datetime="2014-06-24">
										<span class="day">24</span>
										<span class="month-year">
											<span class="month">Jun</span>
											<span class="year">2014</span>
										</span>
									</time>
								
									<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-6.jpg); ">
										<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-6.jpg" class="lazy">
									</div><!-- .img -->
									<div class="content">
										<div class="hgroup">
											<h2>Vivamus Dapibus Volutpat Metus</h2>
											<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
										</div><!-- .hgroup -->
										
										<p>
											Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus interdum tellus tincidunt elit viverra egestas. 
											Suspendisse nulla libero, posuere eget magna nec, porta suscipit felis. Morbi egestas suscipit posuere. Sed at diam nisi. 
											Integer blandit molestie aliquam.
										</p>
										
										<a href="#" class="button white">Continue Reading</a>
										
										<span class="region-tag">Northern Peninsula</span>
									</div><!-- .content -->
								</div><!-- .overview-block -->
								
							</div><!-- .overview-blocks -->
							
						</div><!-- .filter-content -->
						
						
					</div><!-- .filter-area -->

				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>