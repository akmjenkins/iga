<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<div class="hgroup">
						<h1>Search Results</h1>
						<span class="subtitle">16 Results found for <strong><em>Search Query</em></strong></span>
					</div><!-- .hgroup -->

					<h2>News</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar">
						
							<span class="label">
								3 Items Found
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
					<h2>Events</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar">
						
							<span class="label">
								2 Items Found
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
					<h2>Stories</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar">
						
							<span class="label">
								8 Items Found
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

					<h2>Pages</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar">
						
							<span class="label">
								3 Items Found
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
				</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>