define([
		'scripts/components/tim.microtemplate',
		'scripts/map/html.marker',
		'scripts/map/custom.infowindow',
],function(tim,HTMLMarker,CustomInfoWindow) {

	var Map = function(map,json,selectedRegion) {
		var self = this;
		
		this.map = map;
		this.data = json;
		this.infoWindow = CustomInfoWindow.init({},map);
		this.selectedMarker = null;
	
		this.buildMarkers();
		this.buildRegionList();
		this.displayRegion(selectedRegion);
		
		$html.addClass('projects-map-loaded');
		
		var resizeListener = function() {
			var options = $.extend({},$('#projects-map .map').data('map').mapOptions);
			delete options.center;
			delete options.zoom;
			if($(window).outerHeight() < 600) {
				options.zoomControl = false;
				options.mapTypeControl = false;
				delete options.mapTypeOptions;
				delete options.zoomControlOptions;
			}
			map.setOptions(options);
			google.maps.event.trigger(self.map,'resize');
		};	
		
		$(window).on('resize',function() { resizeListener(); });
		resizeListener();
		
		//listeners
		$(document)
			.on('infoWindowAdded',function(e,infoWindow) {
				if(infoWindow === self.infoWindow) {
					self.infoWindow.div.parentsUntil('div.map').addClass('fullsize');
				}
			})
			.on('change','#project-selector',function(e) {
				self.infoWindow.setContent('<div class="has-selector">'+self.getInfoWindowContent(self.selectedMarker,$(this).val())+'</div>');
			})
			.on('click','button.map-close',function(e) {
				self.infoWindow.hide();
			})
			.on('click','button.map-menu',function(e) {
				self.infoWindow.setContent(self.getInfoWindowContent(self.selectedMarker));
			})
			.on('change','#region-list',function(e) {
				var val = $(this).val();
				self.displayRegion( (val !== '' ? self.data.regions[val] : null) );
			});
	
	};

	Map.prototype.toggleMarkers = function(on) {
		var self = this;
		$.each(this.data.markers,function(i,markerObject) {
			markerObject.marker && markerObject.marker.setMap( (on ? self.map : null) );
		});
	};
	
	Map.prototype.fitMarkerBounds = function(markerList) {
		var bounds = new google.maps.LatLngBounds();
		$.each(markerList,function(i,markerObject) {
			markerObject && markerObject.marker && bounds.extend(markerObject.marker.getPosition());
		});
		
		this.map.fitBounds(bounds);
		
	};
	
	Map.prototype.displayRegion = function(regionNameOrObject) {
		var 
			self = this,
			indexOfRegionObject = -1,
			markerList = [],
			regionObject = regionNameOrObject;
		if(typeof regionNameOrObject === 'string') {
			$.each(this.data.regions,function(i,region) {
				if(region.name === regionNameOrObject) {
					indexOfRegionObject = i;
					regionObject = region;
					return false;
				}
			});
		}
		
		//show all regions
		if(!regionObject) {
			this.toggleMarkers(true);
			this.fitMarkerBounds(this.data.markers);
			return;
		}
		
		if(indexOfRegionObject === -1) {
			$.each(this.data.regions,function(i,region) {
				if(region.name === regionObject.name) {
					indexOfRegionObject = i;
					return false;
				}
			});
		}
		
		//turn of all markers
		this.toggleMarkers(false);
		
		$.each(regionObject.places,function(i,place) {
			//turn on the markers for the current region
			var placeObject = self.getPlaceObjectWithId(place);
			if(!placeObject) { return true; }
			
			var marker = placeObject.marker;
			if(!marker) { return true; }
			
			marker.setMap(self.map);
			markerList.push(self.data.markers[place]);
		});
		
		//update the region list selected index
		var regionSelector = $('#region-list');
		var selectedIndex = regionSelector[0].selectedIndex;
		if(selectedIndex != indexOfRegionObject+2) {
			regionSelector[0].selectedIndex = indexOfRegionObject+2; //account for two empty options "Select a Region" and "All Regions"
			regionSelector.trigger('change');
		}
		
		//fit bounds
		this.fitMarkerBounds(markerList);
		
	}

    Map.prototype.getPlaceObjectWithId = function(id) {
        var place = null;
        $.each(this.data.markers,function(i,placeObj) {
            if(placeObj.id == id) {
                place = placeObj;
                return false;
            }
        });

        return place;
    }	
	
	Map.prototype.buildRegionList = function() {
		var options = '<option value="">Select a Region</option>';
		options += '<option value="">All Regions</option>';
		$.each(this.data.regions,function(i,regionObject) {
			if(regionObject.name && regionObject.places.length) {
				options += '<option value="'+i+'">'+regionObject.name+'</option>'
			}
		});
		
		$('#region-list')
			.html(options)
			.trigger('change');
	};
	
	Map.prototype.buildMarkers = function() {
		var self = this;
		$.each(this.data.markers,function(i,markerObject) {
		
			//no position available, we can't map it
			if(typeof markerObject.position !== 'object') { return true; }
		
			var markerTitle = markerObject.place_name;
			markerTitle += ' - '+markerObject.projects.length;
			markerTitle += ' project'+(markerObject.projects.length == 1 ? '' : 's');
			
			var extraClasses = 'map-marker';
			
			if(markerObject.projects.length > 100) {
				extraClasses += ' vlarge';
			} else if(markerObject.projects.length > 50) {
				extraClasses += ' large';
			} else if(markerObject.projects.length > 10) {
				extraClasses += ' medium';
			} else {
				extraClasses += ' small';
			}
			
			
			markerObject.marker = HTMLMarker.init({
				position: new google.maps.LatLng(markerObject.position.lat,markerObject.position.lng),
				map:self.map,
				title:markerTitle,
				extraClasses:extraClasses,
				content:'<span>'+markerObject.projects.length+'</span>'
			});
			
			google.maps.event.addListener(markerObject.marker,'click',function(e) {
				self.infoWindow.setContent(self.getInfoWindowContent(markerObject));
				self.infoWindow.open(markerObject.marker.getPosition());
				self.selectedMarker = markerObject;
			});
			
		});
		
		Map.prototype.getInfoWindowContent = function(markerObject,projectId) {
		
			var windowContent = '<button class="sprite map-close" title="Close">Close</button>';
			windowContent += '<button class="sprite map-menu" title="Return to Project List">Menu</button>';
		
			if(projectId || markerObject.projects.length == 1) {
				windowContent += this.getProjectContent(markerObject,projectId || markerObject.projects[0]);
			} else {
				windowContent += this.getSelectorContent(markerObject);
			}
			
			return windowContent;
		};
		
		Map.prototype.getSelectorContent = function(markerObject) {
			var 
				self = this,
				projectIds = markerObject.projects,
				windowContent = '',
				selectContent = '<option value="">Select a Project</option>';
				
			$.each(projectIds,function(index,projectId) {
				var project = self.data.projects[projectId];
				selectContent += '<option value="'+projectId+'">'+project.project_name+'</option>';
			});
			
			windowContent += '<span class="h2-style">'+markerObject.place_name+'</span>';
			windowContent += '<span class="h4-style">'+projectIds.length+' Project'+(projectIds.length == 1 ? '':'s')+'</span>';
			windowContent += '<select id="project-selector">'+selectContent+'</select>';
			
			return windowContent;
		};
		
		Map.prototype.getProjectContent = function(markerObject,projectId) {
			
			var 
				project = this.data.projects[projectId],
				windowContent = '';
				
				
				windowContent += '<span class="h2-style">'+project.project_name+'</span>';
				windowContent += '<span class="h4-style"><em>Place: '+markerObject.place_name+'</em></span>';
				windowContent += '<span class="h4-style"><em>Organization: '+project.organization_name+'</em></span>';
				if(project.project_type) {
					windowContent += '<span class="h4-style"><em>Type: '+project.project_type+'</em></span>';
				}
				
				if(project.excerpt) {
					windowContent += '<p>'+project.excerpt+'</p>';
				}
				
				if(project.guid) {
					windowContent += '<a href="'+project.guid+'" class="button white">Read More</a>';
				}
				
				return windowContent;
			
		};
	
	};
		
	return Map;
});