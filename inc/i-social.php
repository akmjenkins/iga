<div class="social">						
	<a href="#" class="sprite fb" title="Like International Grenfell Association on Facebook" rel="external">Like International Grenfell Association on Facebook</a>
	<a href="#" class="sprite tw" title="Follow International Grenfell Association on Twitter" rel="external">Follow International Grenfell Association on Twitter</a>
	<a href="#" class="sprite in" title="Connect with International Grenfell Association on LinkedIn" rel="external">Connect with International Grenfell Association on LinkedIn</a>
	<a href="#" class="sprite instagram" title="Follow International Grenfell Association on Instagram" rel="external">Follow International Grenfell Association on Instagram</a>
	<a href="#" class="sprite gplus" title="Add International Grenfell Association to your circles on Google Plus" rel="external">Add International Grenfell Association to your circles on Google Plus</a>
	<a href="#" class="sprite flickr" title="Follow International Grenfell Association on Flickr" rel="external">Follow International Grenfell Association on Flickr</a>
	<a href="#" class="sprite yt" title="Watch International Grenfell Association's YouTube Channel" rel="external">Watch International Grenfell Association's YouTube Channel</a>
	<a href="#" class="sprite pinterest" title="Follow International Grenfell Association on Pinterest" rel="external">Follow International Grenfell Association on Pinterest</a>
</div><!-- .social -->