<div class="interactive-timeline">
	
	<div class="grid-eqh-wrap no-gutters collapse-800">
		<div class="timeline-row grid-eqh">
		
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Dr. Grenfell arrives in Labrador aboard the medical ship, The Albert, on a mission from The Royal National Mission to Deep Sea Fishermen to help support the coastal communities in this area. He begins offering his medical services from a base hospital in Battle Harbour, Labrador, as well as on various ships he purchased to travel along the Labrador coast. Realizing the harsh living conditions and almost complete lack of medical resources in these rural communities, Grenfell is moved to make it his life’s mission to address the needs of these people and their families.
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
			
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Dr. Grenfell arrives in Labrador aboard the medical ship, The Albert, on a mission from The Royal National Mission to Deep Sea Fishermen to help support the coastal communities in this area. He begins offering his medical services from a base hospital in Battle Harbour, Labrador, as well as on various ships he purchased to travel along the Labrador coast. Realizing the harsh living conditions and almost complete lack of medical resources in these rural communities, Grenfell is moved to make it his life’s mission to address the needs of these people and their families.
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
			
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Much shorter content, yet still vertically centered, it's insane!
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
		
		</div><!-- .timeline-row -->
	</div><!-- .grid.eqh-wrap.no-gutters -->

	<div class="grid-eqh-wrap no-gutters collapse-800">
		<div class="timeline-row grid-eqh">
		
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Dr. Grenfell arrives in Labrador aboard the medical ship, The Albert, on a mission from The Royal National Mission to Deep Sea Fishermen to help support the coastal communities in this area. He begins offering his medical services from a base hospital in Battle Harbour, Labrador, as well as on various ships he purchased to travel along the Labrador coast. Realizing the harsh living conditions and almost complete lack of medical resources in these rural communities, Grenfell is moved to make it his life’s mission to address the needs of these people and their families.
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
			
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Dr. Grenfell arrives in Labrador aboard the medical ship, The Albert, on a mission from The Royal National Mission to Deep Sea Fishermen to help support the coastal communities in this area. He begins offering his medical services from a base hospital in Battle Harbour, Labrador, as well as on various ships he purchased to travel along the Labrador coast. Realizing the harsh living conditions and almost complete lack of medical resources in these rural communities, Grenfell is moved to make it his life’s mission to address the needs of these people and their families.
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
			
			<div class="timeline-block col-33">
				<div class="original vcenter">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h4-style">Dr. Grenfell's Arrival</span>
					</div><!-- .ib -->
				</div><!-- .original -->
				<div class="hovered vcenter" style="background-image: url(assets/bin/images/temp/timeline-bg.jpg);">
					<div class="ib">
						<span class="year">1892</span>
						<span class="h2-style">Dr. Grenfell's Arrival</span>
						
						<p>
							Much shorter content, yet still vertically centered, it's insane!
						</p>
					</div><!-- .ib -->
				</div><!-- .hovered -->
			</div><!-- .timeline-block -->
		
		</div><!-- .timeline-row -->
	</div><!-- .grid.eqh-wrap.no-gutters -->

	
</div><!-- .interactive-timeline -->
