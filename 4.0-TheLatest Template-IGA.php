<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<div class="hgroup">
						<h1>The Latest</h1>
						<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
					</div><!-- .hgroup -->

					<h2>Latest News</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar with-arrows">
						
							<span class="label">
								3 News Items
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
					<h2>Upcoming Events</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar with-arrows">
						
							<span class="label">
								2 Upcoming Events
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
					<h2>Latest Stories</h2>
					<div class="filter-area with-swiper">
						<div class="filter-bar with-arrows">
						
							<span class="label">
								8 Stories
							</span><!-- .label -->								
						</div><!-- .filter-bar -->

						<div class="filter-content">
									
							<div class="news-stories-grid">
							
								<div class="swiper-wrapper collapse-599">
									<div class="swipe" data-links="true">
										<div class="swipe-wrap">
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
											<div>											
												<div class="grid-eqh-full-wrap">
													<div class="grid-eqh-wrap collapse-599">
														<div class="grid-eqh">
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh </h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
															<div class="col-33">
																<div class="content">
																	<div class="img">
																		<img src="assets/bin/images/temp/head.jpg" alt="head">
																	</div><!-- .img -->
																	
																	<time class="blocks" datetime="2014-06-24">
																		<span class="day">24</span>
																		<span class="month-year">
																			<span class="month">Jun</span>
																			<span class="year">2014</span>
																		</span>
																	</time>
																	
																	<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
																	
																	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
																	Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
																	
																	<a href="#" class="button">Read More</a>
																	<a href="#" class="button sprite share-white">Share</a>
																
																</div><!-- .padded -->
															</div><!-- .col -->
														</div><!-- .grid-eqh -->
													</div><!-- .grid-eqh-wrap -->
												</div><!-- .grid-eqh-full-wrap -->
											</div>
										</div><!-- .swipe-wrap -->
									</div><!-- .swipe -->
								</div><!-- .swiper-wrapper -->
								
							</div><!-- .news-stories-grid -->

						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
				</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>