				
					<section class="with-flowing-bg who-we-are">
						
						<h2>Who We Are</h2>
						<p>
							Nulla luctus pretium urna eget ullamcorper. Fusce bibendum dolor non auctor dapibus. Proin nec adipiscing libero. 
							Ut sit amet consequat lorem. Donec egestas nibh felis, quis euismod dolor tempus a. Morbi tincidunt risus felis.
						</p>
						
						<a href="#" class="vcenter">
							<span>
								<span class="sprite-before facts-figures">
									Facts &amp; Figures
								</span>
							</span>
						</a><!-- .vcenter -->
						
						<a href="#" class="vcenter">
							<span>
								<span class="sprite-before timeline-ellipsis">
									Interactive Timeline
								</span>
							</span>
						</a><!-- .vcenter -->
						
					</section><!-- .who-we-are -->
