var fs = require('fs');
var sizeOf = require('image-size');

var 
	baseDir = 'src/images/sprites/',
	retinaDir = baseDir+'@2x/';

fs.readdir(baseDir,function(err,files) {
	files.forEach(function(name,index) {
		if(name.match('png')) {
			var 
				regularSize = sizeOf(baseDir+name),
				retinaSize = sizeOf(retinaDir+name);
				
			if(regularSize.width != retinaSize.width/2) {
				console.log(name);
			}
			
			
		}
		
	});
});