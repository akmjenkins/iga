<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<div class="hgroup">
						<h1>Photos &amp; Video</h1>
						<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
					</div><!-- .hgroup -->
					
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-4.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body">
								<div class="article-body">
								
									<p class="centered-excerpt">
										Over the years the IGA has been involved in a number of projects that are applicable to our mandate to serve the needs of the communities 
										of Northern Newfoundland and coastal Labrador. We continue to invest in projects that benefit the people in these areas through grants 
										and bursaries as well as special projects.
									</p>

									<p class="centered-excerpt">
										The International Grenfell Association is looking to the future with a focus on expanding community support beyond funding for projects. 
										Special projects for consideration include:
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body -->
						</div><!-- .cf -->

					<div class="filter-area">
						<div class="filter-bar with-arrows">

							<div class="collapse-labels ib">
							
								<a href="#" id="photos" class="label selected">Photos</a>
								<a href="#" id="videos" class="label">Videos</a>
								
								<div class="selector">
									<select name="filter-selector" id="filter-selector">
										<option value="photos" selected>Photos</option>
										<option value="videos">Videos</option>
									</select>
									<span class="value">&nbsp;</span>
								</div><!-- .selector -->
								
							</div><!-- .collapse-label -->
						</div><!-- .filter-bar -->

						<div class="filter-content">
							<div class="news-stories-grid">
							
								<div class="grid-eqh-full-wrap">
									<div class="grid-eqh-wrap collapse-599">
										<div class="grid-eqh">
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
										</div><!-- .grid-eqh -->
									</div><!-- .grid-eqh-wrap -->
								</div><!-- .grid-eqh-full-wrap -->

								<div class="grid-eqh-full-wrap">
									<div class="grid-eqh-wrap collapse-599">
										<div class="grid-eqh">
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
											<div class="col-33">
												<div class="content">
													<div class="img">
														<img src="assets/bin/images/temp/head.jpg" alt="head">
													</div><!-- .img -->
													
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
													
													<a href="#" class="button">View Photo</a>
													<a href="#" class="button sprite share-white">Share</a>
												
												</div><!-- .content -->
											</div><!-- .col -->
										</div><!-- .grid-eqh -->
									</div><!-- .grid-eqh-wrap -->
								</div><!-- .grid-eqh-full-wrap -->
								
							</div><!-- .news-stories-grid -->
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->
					
				</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>