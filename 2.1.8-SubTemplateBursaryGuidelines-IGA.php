<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Bursary Guidelines</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>International Grenfell Association (IGA) Bursaries are awarded annually to secondary and post-secondary students living in the areas served by the association.</p>
									 
									<p>These awards, supported through annual grants, are given to students based primarily on financial need, but also on academic merit.</p>
									
									<blockquote>
										Please note, as of May 2012, the IGA is no longer awarding scholarships. However, students who were awarded renewable IGA Scholarships up to and including the year 2012 will continue to receive these awards.
									</blockquote>
									
									<h2>Types of Bursaries</h2>
									
									<div class="grid collapse-599">
									
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/money-ico.png" class="aligncenter" alt="money">
												<span class="h3-style">$6,000</span>
												<p>Unlimited, depending on funding available</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/money-ico.png" class="aligncenter" alt="money">
												<span class="h3-style">$4,000</span>
												<p>One annual, for student who has been out of full-time schooling for at least five years and is returning to full-time post-secondary studies.</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
										<div class="col-33">
											<div class="center">
												<img src="assets/bin/images/money-ico.png" class="aligncenter" alt="money">
												<span class="h3-style">$6,000</span>
												<p>Unlimited, depending on funding available</p>
											</div><!-- .center -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
									
									<h2>Criteria for High School Students</h2>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porttitor, purus in facilisis varius, velit leo aliquet risus, non fringilla odio nulla vel velit. 
									Ut consectetur leo augue, vel pharetra lectus lacinia ultrices. In neque dui, volutpat vitae molestie in, suscipit eu neque.</p>
									
									<h2>Criteria for Post-Secondary Students</h2>
									
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porttitor, purus in facilisis varius, velit leo aliquet risus, non fringilla odio nulla vel velit. 
									Ut consectetur leo augue, vel pharetra lectus lacinia ultrices. In neque dui, volutpat vitae molestie in, suscipit eu neque.</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#" class="selected">Bursary Guidlines</a>
										<a href="#">Application Forms</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>