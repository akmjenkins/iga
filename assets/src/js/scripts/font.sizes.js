define([
	'scripts/components/cookies.js'
],
function(cookie) {

		var 
			body = $('body'),
			cookieName = 'jac-wp-font-size',
			defaultSize = 1,
			currentSize = (+cookie(cookieName)) || defaultSize,
			sizes = [
				'smallest',
				'small',
				'large',
				'largest'
			];
		
		$(document)
			.on('setFontSize',function(e) {
				$.each(sizes,function(i,size) { 
					body.removeClass('text-'+size); 
				});
				cookie(cookieName,currentSize,365);
				body.addClass('text-'+sizes[currentSize]);
			})
			.trigger('setFontSize')
			.on('click','#acc-dd .acc-plus, #acc-dd .acc-minus',function(e) {
			
				currentSize += $(this).hasClass('acc-plus') ? 1 : -1;
				
				if(currentSize < 0) {
					currentSize = 0;
				}
				
				if(currentSize >= sizes.length) {
					currentSize = sizes.length-1;
				}
				
				$(document).trigger('setFontSize');
			
				return false;
			});
			
			
		//no public API
		return {};

});