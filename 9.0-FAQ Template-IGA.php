<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
							
						<div class="hgroup">
							<h1>FAQ</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg">
						</div>
						
						<div class="cf">
							<div class="main-body">
								<div class="article-body">
								
									<p class="centered-excerpt">
										The International Grenfell Association was once the major organization providing health care, education, religious services, and other social activities to the fishing industries and costal communities in northern Newfoundland and coastal Labrador.
									</p>
									
									<div class="center">
										
										<h2> Lorem ipsum dolor sit amet consectetur adipiscing elit?</h2>
										
										<p>Our mission is to help grow the health, education, social, and cultural well being of the people of Northern Newfoundland and coastal Labrador, 
										working in partnership with government and other agencies. We are here for you and encourage you to engage our services to help support your community.</p>
										
										<br />
										
										<h2>Fusce nisl magna, malesuada at molestie rutrum, ultrices non erat, Donec ultricies aliquet leo, non scelerisque sapien imperdiet sit amet?</h2>
										
										<p>We exist to foster an ever-increasing sense of social connection and healthy community spirit, and we actively encourage applications that support the development of projects that are in line with this ideal.</p>
										
										<br />
										
										<h2>Nulla faucibus, tellus a tempus tincidunt, enim risus tincidunt purus, in accumsan magna mauris sit amet purus?</h2>
										
										<p>There is no single project or mandate that we will endorse. Rather, every community project brought forward for our consideration will be given full review, and we encourage new ideas for projects that will offer new and better ways of doing things, as well as further enhance the general well being of all members of our communities.</p>
										
									</div>
									
								</div><!-- .article-body -->
							</div><!-- .main-body -->
						</div><!-- .cf -->
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>