<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
							
						<div class="hgroup">
							<h1>Who We Are</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body">
								<div class="article-body">
								
									<p class="centered-excerpt">
										More than a century ago, British surgeon and medical missionary Dr. Wilfred T. Grenfell came to this province with one goal: 
										to serve the many needs of the coastal settlements of Labrador and Northern Newfoundland, especially the fishermen who 
										lived and worked in these areas. Today, Sir Grenfell’s legacy lives on in the International Grenfell Association (IGA), which 
										provides support for local communities through funding for a variety of non-profit organizations.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body -->
						</div><!-- .cf -->
						
						<div class="overview-blocks">
							
							<div class="overview-block with-grad">								
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-1.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-1.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Our History</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										More than a century ago, British surgeon and medical missionary Dr. Wilfred T. Grenfell came to this province 
										with one goal: to serve the many needs of the coastal settlements of Labrador and Northern Newfoundland.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block with-grad">	
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-2.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-2.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Our Role</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										Our role as a registered charitable organization is to empower local non-profit organizations that 
										offer community services by offering them financial support through grants and bursaries.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block with-grad">							
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-3.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-3.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Our Impact</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										To date, the IGA has raised more than $40 million and launched more than 1,000 projects that have helped 
										shape the coastal communities of Labrador and Northern Newfoundland.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block with-grad">	
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-4.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-4.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Leadership</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										Get to know the faces behind the IGA, and learn about what we 
										do for the rural communities of Northern Newfoundland and coastal Labrador.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block with-grad">							
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-5.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-5.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Initiatives</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										Over the years the IGA has been involved in a number of projects that are applicable to 
										our mandate to serve the needs of the communities of Northern Newfoundland and coastal Labrador.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
							<div class="overview-block with-grad">							
								<div class="img" style="background-image: url(assets/bin/images/temp/overview-blocks/ov-block-2.jpg); ">
									<img src="about:blank" data-src="assets/bin/images/temp/overview-blocks/ov-block-2.jpg" class="lazy">
								</div><!-- .img -->
								<div class="content">
									<div class="hgroup">
										<h2>Annual Reports</h2>
										<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
									</div><!-- .hgroup -->
									
									<p>
										Fusce in sollicitudin orci. Phasellus a volutpat augue. Duis in fringilla neque, eget rhoncus lacus. 
										Vestibulum vel diam nec urna dignissim feugiat at vitae nisi. Duis nulla nisi, pharetra nec 
										elementum lobortis, elementum sed erat.
									</p>
									
									<a href="#" class="button white">Continue Reading</a>
								</div><!-- .content -->
							</div><!-- .overview-block -->
							
						</div><!-- .overview-blocks -->
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>