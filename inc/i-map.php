		<div class="projects-map-wrap">
			<div class="map-head">
				<div class="sw">
				
					<form action="/<?php #bloginfo('url'); ?>" class="single-form search-form">
						<fieldset>
							<input type="text" name="subscribe" placeholder="Search International Grenfell Association...">
							<button type="button" class="sprite search-ico-dark" title="Search International Grenfell Association">Search International Grenfell Association</button>
						</fieldset>
					</form><!-- .single-form -->
				
					<div class="selector">
						<select id="region-list">
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
					
					<strong class="sprite-after map-close" title="Close">Close</strong>
					
				</div><!-- .sw -->
			</div><!-- .map-head -->
		
			<div class="gmap" id="projects-map">
				<div class="map" data-zoom="7" data-center="51.90,-55.84" data-markers='[]'></div>
			</div><!-- .gmap -->
		</div><!-- .map-wrap -->