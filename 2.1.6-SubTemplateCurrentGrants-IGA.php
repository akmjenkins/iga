<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Current Grants</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured-2.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>
										Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidunt sapien ullamcorper at. 
										Quisque mollis neque ultrices orci varius rhoncus. Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
										Fusce nec facilisis lorem, id posuere mi.
									</p>
 
									<p>
										Praesent a interdum massa, eget convallis massa. Donec luctus urna quis mauris egestas, a tincidunt mauris dignissim. 
										Duis a nunc non est blandit molestie. Etiam placerat tristique nulla, et sollicitudin augue auctor.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Grant Guidelines</a>
										<a class="selected" href="#">Current Grants</a>
										<a href="#">Completed Grants</a>
										<a href="#">Forms</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->

					</article>
					
					<div class="filter-area with-form extra-margin with-swiper">
						
						<div class="filter-bar">
						
							<span class="label">
								8 Items Found
							</span><!-- .label -->
							
							<div class="selector">
								<select name="filter-selector" id="filter-selector">
									<option value="">Region</option>
									<option value="np">Nothern Peninsula</option>
									<option value="ls">Labrador Straits</option>
									<option value="ls">Labrador South Coast</option>
									<option value="ls">Central Labrador</option>
									<option value="ls">Labrador North Coast</option>
								</select>
								<span class="value">Region</span>
							</div><!-- .selector -->
						
							
							<form action="/" class="filter-form single-form">
								<fieldset>
									<input type="text" name="filter" placeholder="Search grants...">
									<button class="sprite search-ico" title="Search grants...">Search grants...</button>
								</fieldset>
							</form><!-- .single-form -->
							
						</div>
						
						<div class="filter-content with-links">
						
							<div class="links">
								<a href="#" class="sprite previous">Previous</a>
								<a href="#" class="sprite next">Next</a>
							</div><!-- .links -->
							
							<div class="grid-eqh-full-wrap">
								<div class="grid-eqh-wrap pad5 collapse-850">
									
									<div class="grid-eqh">
									
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor quis Vestibulum Placerat</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus 
												interdum tellus tincidunt elit viverra egestas. Suspendisse nulla libero, 
												posuere eget magna nec, porta suscipit felis.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor Vestibulum</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus 
												interdum tellus tincidunt elit viverra egestas.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor quis</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
									</div><!-- .grid-eqh -->
									
								</div><!-- .grid-eqh-wrap -->
							</div><!-- .grid-eqh-full-wrap -->

							<div class="grid-eqh-full-wrap">
								<div class="grid-eqh-wrap pad5 collapse-850">
									
									<div class="grid-eqh">
									
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor quis Vestibulum Placerat</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus 
												interdum tellus tincidunt elit viverra egestas. Suspendisse nulla libero, 
												posuere eget magna nec, porta suscipit felis.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor Vestibulum</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere. Phasellus 
												interdum tellus tincidunt elit viverra egestas.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
										<div class="col-33 dblue-bg">
											<div class="with-region">
												
												<h2>Auctor Tortor quis</h2>
												
												<p>Donec et ipsum mauris. Proin ac massa non tortor ornare posuere.</p>
												
												<a href="#" class="button white">Read More</a>
												
												<span class="region-tag">Northern Peninsula</span>
												
											</div><!-- .with-region -->
										</div><!-- .col -->
										
									</div><!-- .grid-eqh -->
									
								</div><!-- .grid-eqh-wrap -->
							</div><!-- .grid-eqh-full-wrap -->
							
						</div><!-- .filter-content -->
						
						
					</div><!-- .filter-area -->

				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>