define([],function() {
	
	var
		$html = $('html'),
		$button = $('button.toggle-search'),
		$input = $('#nav form.search-form input');
		show = function() { $html.addClass('show-search'); $input.focus(); },
		hide = function() { $html.removeClass('show-search'); $input.blur(); },
		isShowing = function() { return $html.hasClass('show-search'); };
	
	$button.on('click',function() {
		if(isShowing()) {
			return hide();
		}
		
		show();
	});
	
	//hide the search form when we hover over nav items
	$('nav>div>ul>li').on('mouseenter',function() {
		isShowing() && hide();
	});
	
	return {
		show: show,
		hide: hide,
		isShowing:isShowing
	}
	
});