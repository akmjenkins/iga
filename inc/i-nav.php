		<div id="nav">
		
			<nav>
				<div>
					<ul>
						<li>
							<a href="#"><span>Who We Are</span></a>
							<div>
								<ul>
									<li><a href="#">Our History</a></li>
									<li><a href="#">Leadership</a></li>
									<li><a href="#">Our Role</a></li>
									<li><a href="#">Initiatives</a></li>
									<li><a href="#">Our Impact</a></li>
									<li><a href="#">Annual Reports</a></li>
								</ul>
								
								<img src="assets/bin/images/temp/dd1.jpg" alt="house">
							</div>
						</li>
						<li>
							<a href="#"><span>Regions</span></a>
							<div>
								<ul>
									<li><a href="#">Northern Peninsula</a></li>
									<li><a href="#">Labrador Straits</a></li>
									<li><a href="#">Labrador South Coast</a></li>
									<li><a href="#">Central Labrador</a></li>
									<li><a href="#">Labrador North Coast</a></li>
								</ul>
								
								<img src="assets/bin/images/temp/dd2.jpg" alt="house">
							</div>
						</li>
						<li>
							<a href="#"><span>Grants</span></a>
							<div>
								<ul>
									<li><a href="#">Grant Guidelines</a></li>
									<li><a href="#">Current Grants</a></li>
									<li><a href="#">Completed Grants</a></li>
									<li><a href="#">Forms</a></li>
								</ul>
								
								<img src="assets/bin/images/temp/dd1.jpg" alt="house">
							</div>
						</li>
						<li>
							<a href="#"><span>Bursaries</span></a>
							<div>
								<ul>
									<li><a href="#">Bursary Guidelines</a></li>
									<li><a href="#">Application Forms</a></li>
								</ul>
								
								<img src="assets/bin/images/temp/dd1.jpg" alt="house">
							</div>
						</li>
					</ul>
				</div>
				
				<div>
					<ul>
						<li>
							<a href="#"><span>Get Involved</span></a>
							<div>
								<ul>
									<li><a href="#">Volunteer</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
								
							</div>
						</li>
						<li>
							<a href="#"><span>The Latest</span></a>
							<div>
								<ul>
									<li><a href="#">Stories</a></li>
									<li><a href="#">News</a></li>
									<li><a href="#">Events</a></li>
									<li><a href="#">Photos &amp; Videos</a></li>
									<li><a href="#">Newsletter</a></li>

								</ul>
								
							</div>
						</li>
					</ul>
				</div>
				
				<a href="#">
					<span>
						<span class="num">84</span>
						Projects
					</span>
				</a>
				
				<button class="sprite-after abs search-ico replace toggle-search" title="Search IGA">Search IGA</button>
				
			</nav>
			
			<?php include('i-search-form.php'); ?>
			
			<div class="meta-nav">
				<div>
					<a id="acc-item">Accessibility</a>
					<a href="#">Resources</a>
					<a href="#">Login</a>
					<a href="#">Contact</a>
				</div>
				
				<div class="acc-dd" id="acc-dd">
					<div>
						<a href="#" class="sprite-before acc-skip">Skip Navigation</a>
						<a href="#" class="sprite-before acc-font">Text Only</a>
						<a href="#" class="sprite-before acc-plus">Increase Text Size</a>
						<a href="#" class="sprite-before acc-minus">Decrease Text Size</a>
					</div>
				</div><!-- .accessibility -->
			</div><!-- .meta-nav -->
			
			<div class="social-circles">
				<?php include('i-social.php'); ?>
			</div><!-- .social-circles -->
		</div><!-- #nav -->