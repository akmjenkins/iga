<?php
	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<!--[if IE 8 ]>    <html lang="en" class="ie8 resp"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9 resp"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="resp"> <!--<![endif]--> 

	<head>
		<title>International Grenfell Association</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="assets/bin/css/style.css?<?php echo time(); ?>">
		
		<!-- modernizr -->
		<script src="assets/bin/js/lib/modernizr/modernizr.js"></script>
		
		<!-- jQuery -->
		<!--[if IE 8 ]>
			<script src="assets/bin/lib/js/jquery-legacy/jquery.min.js"></script>	
		<![endif]--> 
		<!--[if (gte IE 9)|!(IE)]><!-->
			<script src="assets/bin/lib/js/jquery/jquery.min.js"></script>	
		<!--<![endif]--> 
		
		<!-- magnific popup -->
		<link rel="stylesheet" href="assets/bin/lib/css/magnific-popup/magnific-popup.css?<?php echo time(); ?>">
		<script src="assets/bin/lib/js/magnific-popup/jquery.magnific-popup.min.js?<?php echo time(); ?>"></script>
		
		<!-- favicons -->
		<link rel="icon" type="image/png"  href="assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" type="image/x-icon"  href="assets/bin/images/favicons/favicon.ico">
		<link rel="icon" href="assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon" sizes="152x152" href="assets/bin/images/favicons/apple-touch-152.png">
		<link rel="apple-touch-icon" sizes="144x144" href="assets/bin/images/favicons/apple-touch-144.png">
		<link rel="apple-touch-icon" sizes="120x120" href="assets/bin/images/favicons/apple-touch-120.png">
		<link rel="apple-touch-icon" sizes="114x114" href="assets/bin/images/favicons/apple-touch-114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="assets/bin/images/favicons/apple-touch-144.png">
		<link rel="apple-touch-icon" href="assets/bin/images/favicons/apple-touch-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="assets/bin/images/favicons/apple-touch-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<?php include('i-nav.php'); ?>
		
		<div id="wrapper">			
			<div class="sw">
			
				<header>
					<button id="mobile-nav" class="sprite menu">M</button>
					<a href="#" class="logo replace">International Grenfell Association</a>
				</header>