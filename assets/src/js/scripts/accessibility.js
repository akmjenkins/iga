define([
	'scripts/font.sizes',
	'scripts/plugins/scroll.to'
],function(fontSizes,scrollTo) {

	var
		metaNav = $('#nav div.meta-nav'),
		navItem = $('#acc-item'),
		navMenu =$('#acc-dd'),
		showMenu = function() {
			metaNav.addClass('show-acc');
		},
		hideMenu = function() {
			metaNav.removeClass('show-acc');
		};

	navItem
		.on('mouseenter',function() {
			showMenu();
		})
		.on('mouseleave',function(e) {
			if(!$(e.toElement).closest('#acc-dd').length) {
				hideMenu();
			}
		});
		
	navMenu
		.on('mouseleave',function(e) {
			if(e.toElement !== navItem) {
				hideMenu();
			}
		});
		
	//skipnav
	$('a.acc-skip')
		.on('click',function() {
			$('body').scrollTo('div.body',{
				offsetTop:0
			});
			return false;
		});


	//no public API
	return {};

});