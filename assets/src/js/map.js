//load all required scripts
define([
	'scripts/projects.map'
],function(ProjectMap) {
	
	var 
		map,
		show,
		hide,
		selectedRegion,
		hasBeenLoaded = false,
		mapLoaded = $.Deferred();
		$html = $('html'),
		$projectsmap = $('#projects-map');
		
	if(!$projectsmap.length) { return; }
	
	$(document)
		.on('mapLoad',function(e,map) {
			if(map === $projectsmap.children('div.map').data('map').map) {
				mapLoaded.resolve(map);
			}
		})
		.on('click','div.map-head strong.map-close',function() {
			hide();
		});
		
	show = function() {
		var cb = function() { $projectsmap.closest('div.is-overlay').addClass('show'); };
		if(!hasBeenLoaded) {
			hasBeenLoaded = true;
			$.when(
				mapLoaded.promise(),
				$.ajax(templateJS.templateURL+'map.json')
			).done(function(map,ajaxRes) {
				map = new ProjectMap(map,ajaxRes[0].map,selectedRegion);
				cb.apply();
			}).fail(function() {
			
			});
		} else {
			cb.apply();
		}
	
	};
	
	hide = function() {
		$projectsmap.closest('div.is-overlay').removeClass('show');
	};
	
	//standalone - load automatically
	if(!$projectsmap.closest('div.is-overlay').length) {
		show();
	}
	
	return {
		show:show,
		showWithRegion:function(region) {
			selectedRegion = region;
			show();
		},
		hide:hide
	}
	
});