				<div class="hero">
				
					<div class="swiper-wrapper">
						<div class="swipe" data-controls="true">
							<div class="swipe-wrap">
								<div style="background-image: url(assets/bin/images/temp/hero/rotator-2.jpg);">
									<div>
										<img src="assets/bin/images/temp/hero/rotator-2.jpg">
										
										<div class="content">
											<span class="h2-style">Battle Harbour Historic</span>
											<span class="subtitle">Restoration of the Ben Butt Premises</span>
											
											<a href="#" class="read"><span>Read Story</span></a>
											
										</div><!-- .content -->
									</div>
								</div>
								<div style="background-image: url(assets/bin/images/temp/hero/rotator-1.jpg);">
									<div>
										<img src="assets/bin/images/temp/hero/rotator-1.jpg">
										
										<div class="content">
											<span class="h2-style">Battle Harbour Historic</span>
											<span class="subtitle">Restoration of the Ben Butt Premises</span>
											
											<a href="#" class="read"><span>Read Story</span></a>
											
										</div><!-- .content -->
									</div>
								</div>
								<div style="background-image: url(assets/bin/images/temp/hero/rotator-2.jpg);">
									<div>
										<img src="assets/bin/images/temp/hero/rotator-2.jpg">
										
										<div class="content">
											<span class="h2-style">Battle Harbour Historic</span>
											<span class="subtitle">Restoration of the Ben Butt Premises</span>
											
											<a href="#" class="read"><span>Read Story</span></a>
											
										</div><!-- .content -->
									</div>
								</div>
								<div style="background-image: url(assets/bin/images/temp/hero/rotator-1.jpg);">
									<div>
										<img src="assets/bin/images/temp/hero/rotator-1.jpg">
										
										<div class="content">
											<span class="h2-style">Battle Harbour Historic</span>
											<span class="subtitle">Restoration of the Ben Butt Premises</span>
											
											<a href="#" class="read"><span>Read Story</span></a>
											
										</div><!-- .content -->
									</div>
								</div>
							</div><!-- .swipe-wrap -->
						</div><!-- .swipe -->
					</div><!-- .swiper-wrapper -->
				
					<div class="top module" style="background-image: url(assets/bin/images/temp/hero/top-corner.jpg);">
						<div>
							<img src="assets/bin/images/temp/hero/top-corner.jpg" alt="top">
							<div class="content">
								<p>Rigolet Inuit Community Government – Recreation Equipment and Kitchen Appliances for Community</p>
								<a href="#" class="button white">Read Story</a>
							</div>
						</div>
					</div><!-- .top-module -->
					
					<div class="bottom module" style="background-image: url(assets/bin/images/temp/hero/bottom-corner.jpg);">
						<div>
							<img src="assets/bin/images/temp/hero/bottom-corner.jpg" alt="bottom">
						</div>
					</div><!-- .bottom-module -->
				
				</div><!-- .hero -->
