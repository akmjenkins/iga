(function() {
		if(!Modernizr.firefox) { return; }
		
		
		var 
			throttle = false,
			throttleDelay = 100,
			isCollapsed = function(el) { 
				return el.css('display').toLowerCase() !== 'table-cell'; 
			},
			setGridHeights = function() {
			
				$('div.grid-eqh')
					.each(function() {
						var children = $(this).children();
						if(isCollapsed(children.eq(0))) { return; }
					
						children
							.each(function() {
								var el = $(this).addClass('calculating'); height = el.outerHeight();
								el.data('height',height);
							})
							.each(function() {
								var 
									el = $(this).removeClass('calculating'),
									firefoxRel = el.data('firefox-rel'),
									height = el.data('height');
									
									if(firefoxRel) {
										firefoxRel.css('height',height+'px');
									} else {
										if(height) {
											el.data('firefox-rel',el.wrapInner('<div class="firefox-rel" style="height:'+height+'px"/>').children('div.firefox-rel'));
										}
									}								
							});
				});
			
			};
			
			$(window).on('resize',function() {
				clearTimeout(throttle);
				throttle = setTimeout(function() {
					setGridHeights();
				},throttleDelay);
			});
			
			setGridHeights();
		
}());