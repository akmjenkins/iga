			</div><!-- .sw -->	
			<footer>
				<div class="sw">
				
					<div class="grid-eqh-full-wrap collapse-900">
						<div class="grid-eqh-wrap">
							
							<div class="grid-eqh no-margin">
								<div class="col-33 footer-block">
									<div>
										<span class="uc block">International Grenfell Association</span>
										<address>
											81 Kenmount Road, 2nd Floor <br />
											St. John's, NL, Canada <br />
											A1B 3P8
										</address>
										
										<div class="bottom-block">
										
											<a href="#" class="button white">Contact Us</a>
										
											<div class="indent-row">
												<span>Tel</span> 709 745 6162
											</div><!-- .indent-row -->
											
											<div class="indent-row">
												<span>Fax</span> 709 745 6163
											</div><!-- .indent-row -->
										
										</div><!-- .block-bottom -->
									</div>
								</div><!-- .col -->
								
								<div class="col-33 footer-block links">
									<div>
										<ul>
											<li><a href="#">Who We Are</a></li>
											<li><a href="#">Bursaries</a></li>
											<li><a href="#">Regions</a></li>
											<li><a href="#">The Latest</a></li>
											<li><a href="#">Grants</a></li>
											<li><a href="#">Ways to Give</a></li>
										</ul>
										
										<div class="bottom-block">
											<div class="social-white">
											<?php include('i-social.php'); ?>
											</div><!-- .social-white -->
										</div><!-- .bottom-block -->
									</div>
								</div><!-- .col -->
								
								<div class="col-33 footer-block subscribe">
									<div>
										<span class="h2-style">Subscribe</span>
										<p>Cras luctus porttitor imperdiet. In imperdiet lectus at massa commodo ullamcorper.</p>
									
										<div class="bottom-block">
											<?php include('i-subscribe-form.php'); ?>
										</div><!-- .bottom-block -->
									</div>
								</div><!-- .col -->
							</div><!-- .grid-eqh -->
							
						</div><!-- .grid-eqh-wrap -->
					</div><!-- .grid-eqh-full-wrap -->
				
					<div class="copyright">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">International Grenfell Association</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac" class="replace">JAC. We Create.</a>
					</div><!-- .copyright -->
				</div><!-- .sw -->
			</footer><!-- .footer -->
	
		</div><!-- #wrapper -->
		
		<script>
			var templateJS = {
				templateURL: 'http://jac.dev/iga/',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script data-main="assets/bin/js/main.js" src="assets/bin/lib/js/requirejs/require.min.js"></script>
	</body>
</html>