<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Our Role</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="featured-image">
							<img src="assets/bin/images/temp/featured.jpg" alt="featured">
						</div>
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>The International Grenfell Association was once the major organization providing health care, 
									education, religious services, and other social activities to the fishing industries and costal 
									communities in northern Newfoundland and coastal Labrador.</p>
									 
									<p>The IGA prides itself on continuing the tradition of excellence in community care that Sir Grenfell 
									brought with him to the shores of this province.</p>
									 
									<p>Today our role as a registered charitable organization is to empower local non-profit organizations that offer 
									community services by offering them financial support through grants and bursaries.</p>
									 
									<p>Our core values rest on the traditional values originally expressed in the first years of the Grenfell mission:</p>
									 
									<h2>Service</h2>
									 
									<p>Our mission is to help grow the health, education, social, and cultural well being of the people of Northern 
									Newfoundland and coastal Labrador, working in partnership with government and other agencies. We are here for 
									you and encourage you to engage our services to help support your community.</p>
									 
									<h2>Community building</h2>
									 
									<p>We exist to foster an ever-increasing sense of social connection and healthy community spirit, and we actively 
									encourage applications that support the development of projects that are in line with this ideal.</p>
									 
									<h2>Innovation</h2>
									 
									<p>There is no single project or mandate that we will endorse. Rather, every community project brought forward for 
									our consideration will be given full review, and we encourage new ideas for projects that will offer new and better 
									ways of doing things, as well as further enhance the general well being of all members of our communities.</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#">Our History</a>
										<a href="#" class="selected">Our Role</a>
										<a href="#">Our Impact</a>
										<a href="#">Leadership</a>
										<a href="#">Initiatives</a>
										<a href="#">Annual Reports</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
				
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>