<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<div class="hgroup">
						<h1>Events</h1>
						<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
					</div><!-- .hgroup -->
					
					
					<h2>Featured</h2>
					<div class="featured-story">
						<div class="img" style="background-image: url(assets/bin/images/temp/story-image.jpg);">
							<img src="about:blank" data-src="assets/bin/images/temp/story-image.jpg" alt="story-image" class="lazy">
						</div><!-- .img -->
						
						<time class="blocks dual-colored" datetime="2014-06-24">
							<span class="day">24</span>
							<span class="month-year">
								<span class="month">Jun</span>
								<span class="year">2014</span>
							</span>
						</time>
						
						<div class="content">
						
							<h2>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h2>
							<p>Praesent consectetur augue leo, quis ultricies orci porta ut. Cras vehicula nisl ligula, ut tincidunt sapien ullamcorper at. 
							Quisque mollis neque ultrices orci varius rhoncus. Praesent euismod libero sed est varius, ac pharetra lectus eleifend. 
							Fusce nec facilisis lorem, id posuere mi.</p>
							
							<a href="#" class="button white">Read More</a>
							
							<a href="#" class="button white sprite share-blue">Share</a>
							
						</div>
						
					</div><!-- .featured-story -->
					
					<h2>Upcoming Events</h2>
					
					<div class="filter-area with-form with-swiper">
						
						<div class="filter-bar">
						
							<span class="label">
								8 Items Found
							</span><!-- .label -->
							
							<div class="collapse-labels ib">
							
								<a href="#" class="label">The Latest</a>
								<a href="#" class="label">Most Popular</a>
								
								<div class="selector">
									<select name="the-latest-region" id="the-latest-region">
										<option value="">Type</option>
										<option value="the-latest">The Latest</option>
										<option value="most-popular">Most Popular</option>
									</select>
									<span class="value">Type</span>
								</div><!-- .selector -->
							
							</div><!-- .collapse-labels -->
						
							
							<form action="/" class="filter-form single-form">
								<fieldset>
									<input type="text" name="filter" placeholder="Search grants...">
									<button class="sprite search-ico" title="Search grants...">Search grants...</button>
								</fieldset>
							</form><!-- .single-form -->
							
						</div><!-- .filter-bar -->

						<div class="filter-content">
							<div class="cf">
								<div class="main-body with-sidebar full">
									
									<!-- the class of .with-links denotes the immediate parent of the .links div, necessary for correct responsive positioning of the .links div -->
									<div class="news-stories-grid with-links">
									
										<div class="links">
											<a href="#" class="sprite previous">Previous</a>
											<a href="#" class="sprite next">Next</a>
										</div><!-- .links -->
									
										<div class="grid-eqh-full-wrap">
											<div class="grid-eqh-wrap collapse-599">
												<div class="grid-eqh">
													<div class="col-50">
														<div class="content">
															<div class="img">
																<img src="assets/bin/images/temp/head.jpg" alt="head">
															</div><!-- .img -->
															
															<time class="blocks" datetime="2014-06-24">
																<span class="day">24</span>
																<span class="month-year">
																	<span class="month">Jun</span>
																	<span class="year">2014</span>
																</span>
															</time>
															
															<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<a href="#" class="button">Read More</a>
															<a href="#" class="button sprite share-white">Share</a>
														
														</div><!-- .padded -->
													</div><!-- .col -->
													<div class="col-50">
														<div class="content">
															<div class="img">
																<img src="assets/bin/images/temp/head.jpg" alt="head">
															</div><!-- .img -->
															
															<time class="blocks" datetime="2014-06-24">
																<span class="day">24</span>
																<span class="month-year">
																	<span class="month">Jun</span>
																	<span class="year">2014</span>
																</span>
															</time>
															
															<h3>Fusce nec Nibh </h3>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
															
															<a href="#" class="button">Read More</a>
															<a href="#" class="button sprite share-white">Share</a>
														
														</div><!-- .padded -->
													</div><!-- .col -->
												</div><!-- .grid-eqh -->
											</div><!-- .grid-eqh-wrap -->
										</div><!-- .grid-eqh-full-wrap -->

										<div class="grid-eqh-full-wrap">
											<div class="grid-eqh-wrap collapse-599">
												<div class="grid-eqh">
													<div class="col-50">
														<div class="content">
															<div class="img">
																<img src="assets/bin/images/temp/head.jpg" alt="head">
															</div><!-- .img -->
															
															<time class="blocks" datetime="2014-06-24">
																<span class="day">24</span>
																<span class="month-year">
																	<span class="month">Jun</span>
																	<span class="year">2014</span>
																</span>
															</time>
															
															<h3>Fusce nec Nibh Scelerisque Neque Gravida Sodales</h3>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<a href="#" class="button">Read More</a>
															<a href="#" class="button sprite share-white">Share</a>
														
														</div><!-- .padded -->
													</div><!-- .col -->
													<div class="col-50">
														<div class="content">
															<div class="img">
																<img src="assets/bin/images/temp/head.jpg" alt="head">
															</div><!-- .img -->
															
															<time class="blocks" datetime="2014-06-24">
																<span class="day">24</span>
																<span class="month-year">
																	<span class="month">Jun</span>
																	<span class="year">2014</span>
																</span>
															</time>
															
															<h3>Fusce nec Nibh </h3>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. </p>
															
															<a href="#" class="button">Read More</a>
															<a href="#" class="button sprite share-white">Share</a>
														
														</div><!-- .padded -->
													</div><!-- .col -->
												</div><!-- .grid-eqh -->
											</div><!-- .grid-eqh-wrap -->
										</div><!-- .grid-eqh-full-wrap -->
										
									</div><!-- .news-stories-grid -->
									
								</div><!-- .main-body.with-sidebar -->
								
								<aside class="sidebar">
									
									<div>
										<div class="accordion">
											
											<div class="accordion-item">
												<div class="accordion-item-handle">
													<span class="num">15</span> 2014
												</div><!-- .accordion-item-handle -->
												<div class="accordion-item-content">
													<ul>
														<li><a href="#">June (2)</a></li>
														<li><a href="#">May (3)</a></li>
														<li><a href="#">April (4)</a></li>
														<li><a href="#">March (3)</a></li>
													</ul>
												</div><!-- .accordion-item-content -->
											</div><!-- .accordion-item -->
											
											<div class="accordion-item">
												<div class="accordion-item-handle">
													<span class="num">15</span> 2013
												</div><!-- .accordion-item-handle -->
												<div class="accordion-item-content">
													<ul>
														<li><a href="#">June (2)</a></li>
														<li><a href="#">May (3)</a></li>
														<li><a href="#">April (4)</a></li>
														<li><a href="#">March (3)</a></li>
													</ul>
												</div><!-- .accordion-item-content -->
											</div><!-- .accordion-item -->
											
											<div class="accordion-item">
												<div class="accordion-item-handle">
													<span class="num">15</span> 2012
												</div><!-- .accordion-item-handle -->
												<div class="accordion-item-content">
													<ul>
														<li><a href="#">June (2)</a></li>
														<li><a href="#">May (3)</a></li>
														<li><a href="#">April (4)</a></li>
														<li><a href="#">March (3)</a></li>
													</ul>
												</div><!-- .accordion-item-content -->
											</div><!-- .accordion-item -->
											
										</div><!-- .accordion -->
									</div>
									
								</aside><!-- .sidebar -->
							</div><!-- .cf -->
						</div><!-- .filter-content -->
						
					</div><!-- .filter-area -->

				</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>