<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
				
				<div class="body">
					<article>
						<div class="hgroup">
							<h1>Northern Peninsula</h1>
							<span class="subtitle">Fusce nec Nibh Scelerisque Neque</span>
						</div><!-- .hgroup -->
						
						<div class="cf">
							<div class="main-body with-sidebar">
								<div class="article-body">
								
									<p>
										More than a century ago, British surgeon and medical missionary Dr. Wilfred T. Grenfell came to this province with one goal: 
										to serve the many needs of the coastal settlements of Labrador and Northern Newfoundland, especially the fishermen who 
										lived and worked in these areas. Today, Sir Grenfell’s legacy lives on in the International Grenfell Association (IGA), which 
										provides support for local communities through funding for a variety of non-profit organizations.
									</p>
									
								</div><!-- .article-body -->
							</div><!-- .main-body.with-sidebar -->
							
							<aside class="sidebar">
								
								<div>
									<div class="related-links">
										<a href="#" class="selected">Northern Peninsula</a>
										<a href="#">Labrador Straits</a>
										<a href="#">Labrador South Coast</a>
										<a href="#">Central Labrador</a>
										<a href="#">Labrador North Coast</a>
									</div>
								</div>
								
							</aside><!-- .sidebar -->
							
						</div><!-- .cf -->
					</article>
					
					<div class="map-wrap">
						<div class="map" data-zoom="9" data-center="51.116, -55.70" data-markers='[{"title":"Marker 1","position":"51.392,-55.54","image":"assets/bin/images/map-pin.png"},{"title":"Marker 2","position":"51.432,-56.45","image":"assets/bin/images/map-pin.png"},{"title":"Marker 3","position":"51.543,-55.67","image":"assets/bin/images/map-pin.png"}]'></div>
					</div><!-- .map-wrap -->
						
				</div><!-- .body -->
				
<?php include('inc/i-footer.php'); ?>