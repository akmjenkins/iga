# README.md

## Grunt, GIT, and Front End Assets

Make sure you have `git`, `nodejs` (and `npm` - comes with node, usually) installed.

### Git
* [Download Page](http://git-scm.com/downloads)
* [Windows](http://git-scm.com/download/win)
* [Mac](http://git-scm.com/download/mac)

### Node
* [Download Page](http://nodejs.org/download/)
* [Windows - v0.10.28](http://nodejs.org/dist/v0.10.28/node-v0.10.28-x86.msi)
* [Mac - 10.10.28](http://nodejs.org/dist/v0.10.28/node-v0.10.28.pkg)

##Instructions
* Open up terminal
* Create a new directory and run
`git clone https://bitbucket.org/akmjenkins/projectname ./projectname` (checks out the tree into a folder `./projectname` - the precise name of the folder doesn't matter.)
* Navigate to `./projectname/assets/`
* Run `npm install` to install all dependencies. 
* Run `npm install grunt-cli -g` (`sudo` if not on Windows) to install the grunt command line tool globally (you only need to install this tool once on your machine and it will be available everywhere)
* Run `grunt bower` to install all dependencies of this project (listed in `bower.json`)

## For development
* Inside the `assets` directory run `grunt dev-watch`
* Any time you edit/add a CSS file in `src/css` - `bin/style.css` will be recompiled automatically.
* JS is concatenated and minified using [requirejs](http://requirejs.org/) - take a look at `src/js/main.js` and the files it requires to get a handle on the `define` and `require` functions.
* Any time you update `src/js/main.js` - `bin/js/main.js` will be recompiled automatically

## For distribution
* Inside the `assets` directory run `grunt prod`
* Only ever upload `assets/bin` to the live server
* Note: You'll need to put your [TinyPNG API Key](https://tinypng.com/developers) in `assets/user.defaults.json` in order to get TinyPNG to losslessly compress any PNG image assets (e.g. sprites) in the `assets/src` folder

## GitHub
Sign up for a [free Github Account](https://github.com/join) if you don't already have one. Let me know your username and I'll add you as a collaborator to this repo.

### Notes
* **NEVER** edit/add anything inside the `assets/bin` directory - you will lose it.
* **ALWAYS** commit your work to the repo. 

### Fun GitHub URLs
* https://help.github.com/articles/fetching-a-remote
* https://www.atlassian.com/git/tutorial/remote-repositories
* http://barkingiguana.com/2008/11/20/working-on-other-peoples-projects-with-git/
